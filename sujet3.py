tab=[[True,False,False,False],
     [False,True,True,False]]

class Case():
    def __init__(self,i=-1,j=-1):
        self.i=i
        self.j=j



def voisinCase(list,case=Case):
    liste=[]
    maxvertical=len(list)
    maxhorizontal=len(list[0])
    if (case.i-1>=0 ) :
        if  (list[case.i-1][case.j]==False):
            casetemp=Case(case.i-1,case.j)
            liste.append(casetemp)
    if(case.i+1<maxvertical ):
        if (list[case.i+1][case.j]==False):
            casetemp = Case(case.i + 1, case.j)
            liste.append(casetemp)

    if (case.j - 1 >= 0) :
            if (list[case.i][case.j - 1]==False):
                casetemp = Case( case.i,case.j - 1)
                liste.append(casetemp)
    if (case.j + 1 < maxhorizontal) :
            if  (list[case.i][case.j+ 1]==False):
                casetemp = Case(case.i,case.j + 1)
                liste.append(casetemp)
    return liste






def testExist(listvoisin, case=Case):
    exist=False
    for k in range(0 , len(listvoisin)):
        if(case.i == listvoisin[k].i and case.j==listvoisin[k].j):
            return  True
    return exist
def ensemblevoisin(listetotal ,listecase):
    ensemblevoisin=[]
    for m in range(0,len(listecase)):
        listevoisin=voisinCase(listetotal,listecase[m])
        for n in range(0,len(listevoisin)):
            if not (testExist(ensemblevoisin,listevoisin[n])):
                ensemblevoisin.append(listevoisin[n])
    return ensemblevoisin


def accessible(listtotal, case=Case):
    listevoisincaseinital=voisinCase(listtotal,case)
    ensembleAccessible=listevoisincaseinital
    k=len(listtotal)+len(listtotal[0])-2
    while True:
        ensemblevoisinTemp=ensemblevoisin(listtotal,ensembleAccessible)
        if k==0:
            break
        for i in range(0, len(ensemblevoisinTemp)):
            if not (testExist(ensembleAccessible,ensemblevoisinTemp[i])):
                ensembleAccessible.append(ensemblevoisinTemp[i])
        k-=1
    return ensembleAccessible

def chemin(listTotal,deb=Case,fin=Case):
    listeAccessibledeb=accessible(listTotal,deb)
    listeAccessiblefin=accessible(listTotal,fin)
    test=False
    i=0
    while not test and i<len(listeAccessiblefin):
        if(testExist(listeAccessibledeb,listeAccessiblefin[i])):
            test=True
        i+=1
    return test



case1=Case(1,2)
case2=Case(0,1)
print(chemin(tab,case1,case2))


case1=Case(0,2)
case2=Case(1,0)
print(chemin(tab,case1,case2))



